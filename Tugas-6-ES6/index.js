//No1
console.log('======= No 1 =======')
let keliling = (p,l) => 2+(p * l);
let luas = (p,l) => p * l;
console.log('Kelilingnya = ' +keliling (2,3));
console.log('Luasnya = ' +luas (2,3));




//no2
console.log('======= No 2 =======')
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
        const ful = `${firstName} ${lastName}`
      console.log(ful)
    }
  }
}
 
//Driver Code 
newFunction("indra", "Lesmana").fullName() 



//no3
console.log('======= No 3 =======')

const newObject = {
    firstName: "Indra",
    lastName: "Lesmana",
    address: "Kp. Ubrug RT 02 RW 06 Kecamatan Warungkiara Kabupaten Sukabumi",
    hobby: "playing football",
  }
  const {firstName, lastName,address,hobby} = newObject

  console.log(firstName, lastName, address, hobby)


  //no4
  console.log('======= No 4 =======')

  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  let combinedArray = [...west, ...east]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']


//no 5
console.log('======= No 5 =======')

const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` 
console.log(before)

