//soal nomor 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var saya = pertama.substr(0,4);
var senang = pertama.substr(12,6);
var belajar = kedua.substr(0,7);
var javascript =kedua.substr(8,10).toUpperCase();
var hasilakhir = saya+ " " +senang+" "+belajar+" "+javascript;
console.log(hasilakhir);

//soal nomor 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
var nilai1 = parseInt(kataPertama);
var nilai2 = parseInt(kataKedua);
var nilai3 = parseInt(kataKetiga);
var nilai4 = parseInt(kataKeempat);
var hasil = (nilai1-(nilai2+nilai3))*nilai4;
console.log(hasil);

//soal nomor 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua=kalimat.substr(4,10)
var kataKetiga=kalimat.substr(15,3); // do your own! 
var kataKeempat=kalimat.substr(19,5); // do your own! 
var kataKelima=kalimat.substr(25,6); // do your own! 
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);