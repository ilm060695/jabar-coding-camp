var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(readBooksPromise(10000,books[0]))
    }, 10000)
  })
var promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(readBooksPromise(10000,books[1]))
    }, 10000)
  })
var promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(readBooksPromise(10000,books[2]))
    }, 10000)
  })
var promise4 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(readBooksPromise(10000,books[3]))
    }, 10000)
  })